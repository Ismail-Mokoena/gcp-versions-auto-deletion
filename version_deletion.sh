#!/bin/bash 

##Today's date(YYY-mm-dd)## 
now="$(date "+%Y-%m-%d")" 

## Filter the versions, return only those that are 1-day old and have no traffic allocated to them. ## 
VERSION_F=$(gcloud app versions list --filter="version.createTime.date('%Y-%m-%d%', Z) < '$now' AND traffic_split=0" --format 'value(version.id)')
##Loop through version id's and delete##
for version_id in $VERSION_F
do
    gcloud app versions delete "$version_id"
done